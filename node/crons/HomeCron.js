const cron = require('node-cron');
const { HomeService } = require('../services/HomeService');

class HomeCron {
  constructor() {
    this.homeService = new HomeService();
  }
    
  start() {
    let homeService = this.homeService;
    cron.schedule('*/5 * * * * *', function() {
      console.log(homeService.getHomes());
    });
  }
}

module.exports = { HomeCron };