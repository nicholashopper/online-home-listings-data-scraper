
const csv = require('csvtojson');
const axios = require('axios');


class RedfinAdapter {
  constructor() {
    this.csvResponse = null;
    this.homes = [];
  }
    
  getHomes() {
    this._fetchRawData()
    .then(() => this._parseResponse());
  }

  _parseResponse() {
    console.log(2);
    csv(
      {
        flatKeys:true,
        headers: ['saleType','soldDate','propertyType','address','city','state','zip',
          'price','beds','baths','location','squareFeet','lotSize','yearBuilt',
          'daysOnMarket','pricePerSquareFoot','hoaMonthlyFee','status',
          'nextOpenHouseStartTime','nextOpenHouseEndTime','url','source','mlsNumber',
          'favorite','interested','lat','long'],
        colParser: {
          price: 'number',
          beds: 'number',
          baths: 'number',
          squareFeet: 'number',
          lotSize: 'number',
          yearBuilt: 'number',
          pricePerSquareFoot: 'number',
          daysOnMarket: 'number',
          favorite:  (item) => item === 'Y',
          interested:  (item) => item === 'Y',
          lat: 'number',
          long: 'number'
        }
      }
    )
    .fromString(this.csvResponse)
    .subscribe((jsonObj)=>{
      console.log(jsonObj);
    })
  }
  
  async _fetchRawData() {
    const response = await axios.get('https://www.redfin.com/stingray/api/gis-csv?al=1&market=sanantonio&min_stories=1&num_homes=3&ord=redfin-recommended-asc&page_number=1&region_id=16657&region_type=6&sf=1,2,3,5,6,7&status=9&time_on_market_range=1-&uipt=1,2,3,4,5,6&v=8');
    this.csvResponse = response.data;
  }
}

module.exports = { RedfinAdapter };