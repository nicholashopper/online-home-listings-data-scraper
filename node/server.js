// index.js
const config = require('./config/config.js');
const { HomeCron } = require('./crons/HomeCron.js');
const express = require("express");
const fs = require("fs");
process.env.NODE_ENV = 'development';

app = express();

app.get('/config', (req, res) => {
  res.json(global.gConfig);
});

app.listen(global.gConfig.node_port, () => {
  console.log(`${global.gConfig.config_id}: ${global.gConfig.app_name} listening on port ${global.gConfig.node_port}`);
});

const crons = new Array(new HomeCron());
crons.forEach((crons) => crons.start());



