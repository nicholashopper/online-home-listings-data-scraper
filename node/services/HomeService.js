const { RedfinAdapter } = require('../adapters/RedfinAdapter');

class HomeService {
  constructor() {
    this.redfinAdapter = new RedfinAdapter();
  }
    
  async getHomes() {
    return await this.redfinAdapter.getHomes();
  }
}

module.exports = { HomeService };